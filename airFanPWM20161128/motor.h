class motor{
  public:
    int pin;
    int value;
    void setValue(int _value){value=_value;}
    void setup(int _pin){
      pin=_pin;
      pinMode(pin,OUTPUT);
      analogWrite(pin,value);
      value=0;
      }
    void loop(){analogWrite(pin,value);}
};

